import faker

from django.contrib.auth import get_user_model


class TestMixin:
    """ Test mixin """
    faker = faker.Faker()

    def get_profile(self):
        return self.faker.simple_profile()

    def get_or_create_user(self):
        profile = self.get_profile()
        user, _ = get_user_model().objects.get_or_create(
            username=profile['username'],
            password=profile['username'],
        )
        return user

    def get_or_create_superuser(self):
        profile = self.get_profile()
        user, _ = get_user_model().objects.get_or_create(
            username=profile['username'],
            password=profile['username'],
            is_superuser=True,
        )
        return user
